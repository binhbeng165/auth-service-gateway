import { Inject, Injectable } from '@nestjs/common';
import { CreateSongDto } from './dto/create-song.dto';
import { UpdateSongDto } from './dto/update-song.dto';
import { PrismaService } from 'src/database/prisma.service';
import { ClientProxy } from '@nestjs/microservices';
import { firstValueFrom } from 'rxjs';

@Injectable()
export class SongService {
  constructor(
    @Inject('SONG_SERVICE') private readonly authClient: ClientProxy,
    private readonly prisma: PrismaService,
    // private readonly redisService: RedisService,
  ) {
    this.authClient.connect();
  }
  async create(createSongDto: CreateSongDto) {
    const created = await firstValueFrom(
      this.authClient.send(
        'create-band',
        {
          // headers: { 'x-elastic-apm-traceparent': apm.currentTraceparent },
        }),
    );
    return created
  }

  findAll() {
    return `This action returns all song`;
  }

  findOne(id: number) {
    return `This action returns a #${id} song`;
  }

  update(id: number, updateSongDto: UpdateSongDto) {
    return `This action updates a #${id} song`;
  }

  remove(id: number) {
    return `This action removes a #${id} song`;
  }
}
