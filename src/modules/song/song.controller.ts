import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Inject,
} from '@nestjs/common';
import { Public } from 'src/core/decorators/allow.decorator';
import { ClientProxy, MessagePattern } from '@nestjs/microservices';
import { firstValueFrom } from 'rxjs';
import { ISong } from './interfaces/ISong';
@Public()
@Controller('song')
export class SongController {
  constructor(
    @Inject('SONG_SERVICE') private readonly songClient: ClientProxy,
  ) {
    this.songClient.connect();
  }

  @Post('create-song')
  async create(@Body() song: ISong) {
    const created = await firstValueFrom(
      this.songClient.send('song.create', {
        song,
        // headers: { 'x-elastic-apm-traceparent': apm.currentTraceparent },
      }),
    );
    return created;
  }

  @MessagePattern('song.create')
  findAll() {
    console.log('hihi');
  }

  @Get(':id')
  findOne(@Param('id') id: string) {}

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateSongDto: any) {}
}
