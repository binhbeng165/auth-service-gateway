export interface ISong {
  name: string;
  lyrics: string;
  releaseYear: string;
  image?: string;
  chords: string[];
  composerIds?: number[];
  artistIds?: number[];
  bandIds?: number[];
  emptyCheck?: null;
}
