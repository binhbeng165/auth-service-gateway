import {
  BadRequestException,
  HttpException,
  HttpStatus,
  Inject,
  Injectable,
} from '@nestjs/common';
import { ClientProxy, MessagePattern, Payload } from '@nestjs/microservices';
import { Prisma, User } from '@prisma/client';
import { VerifyDto, GetOtpDto, LoginDto, CreateUserDto } from './dto';
import { PrismaService } from 'src/database/prisma.service';
import { JwtService } from '@nestjs/jwt';
import { JwtPayload } from 'src/types';
import { ConfService } from 'src/configurations/conf.service';
import { EnvService } from 'src/configurations/env.service';
import * as apm from 'elastic-apm-node';

@Injectable()
export class AuthService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly jwtService: JwtService,
    private readonly conf: ConfService,
  ) {}

  public async login(data: LoginDto) {
    try {
      const { phone, password } = data;
      const user = await this.prisma.user.findUnique({ where: { phone } });
      const payload: JwtPayload = {
        id: user.id,
        scope: 'default',
        roles: ['User'],
        permissions: ['create'],
      };
      const token = this.jwtService.sign(payload, {
        secret: this.conf.get<string>('JWT_SECRET'),
        expiresIn: this.conf.get('JWT_EXPIRE'),
      });
      return {
        user,
        token,
      };
    } catch (e) {
      throw new BadRequestException(e.message);
    }
  }

  public getUserById(id: number) {
    const transaction = apm.startTransaction(`auth.getUserById`, 'event');
    const findSpan = transaction.startSpan('find-database');
    let user = this.prisma.user.findUnique({ where: { id } });
    findSpan.end();
    transaction.end();
    return user;
  }
}
