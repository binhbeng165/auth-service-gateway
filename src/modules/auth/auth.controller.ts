import {
  Body,
  Controller,
  Get,
  HttpCode,
  Param,
  Post,
  UseGuards,
} from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { User } from '@prisma/client';
import { AuthService } from './auth.service';
import { CurrentUser, JwtAuthGuard, Public } from '../../core';
import { LoginDto } from './dto';
import * as apm from 'elastic-apm-node';
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Public()
  @Post('/login')
  login(@Body() data: LoginDto) {
    return this.authService.login(data);
  }

  @MessagePattern('get_user_by_id_in_controller')
  public async getUserById(@Payload() data: any): Promise<User> {
    const apmTransaction = apm.startTransaction('get-user-by-id', 'rabbitmq', {
      childOf: data.headers['x-elastic-apm-traceparent'],
    });
    let p = this.authService.getUserById(1);
    apmTransaction.end();
    return p;
  }
}
