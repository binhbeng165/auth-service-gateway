import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { join } from 'path';
import { APP_GUARD } from '@nestjs/core';
import { TerminusModule } from '@nestjs/terminus';
// import { AcceptLanguageResolver, I18nModule, QueryResolver } from 'nestjs-i18n';
import { JwtAuthGuard, RolesGuard } from './core';
import { PassportModule } from '@nestjs/passport';
import { AppController } from './app.controller';
import { AuthModule } from './modules/auth/auth.module';
import { EnvService } from 'src/configurations/env.service';
import { PrismaService } from 'src/database/prisma.service';
import { ConfigurationModule } from './configurations/configurations.module';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { ConfService } from './configurations/conf.service';
import { TracingMiddleware } from './core/middlewares/trace.middleware';
import { LogService } from './logger/logger.service';
import { RabbitMQModule } from './rabbitmq/rabbitmq.module';
import { RequestlogModule } from './requestlog/requestlog.module';
import { SongModule } from './modules/song/song.module';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { SongController } from './modules/song/song.controller';
@Module({
  imports: [
    ConfigurationModule,
    AuthModule,
    RabbitMQModule,
    PassportModule.register({ defaultStrategy: 'jwt' }),
    // I18nModule.forRoot({
    //   fallbackLanguage: 'en',
    //   loaderOptions: {
    //     path: join(__dirname, '/i18n/'),
    //     watch: true,
    //   },
    //   resolvers: [
    //     { use: QueryResolver, options: ['lang'] },
    //     AcceptLanguageResolver,
    //   ],
    // }),

    TerminusModule,
    RequestlogModule,
    // SongModule,
    ClientsModule.registerAsync([
      {
        name: 'SONG_SERVICE',
        useFactory: async (env: EnvService) => ({
          transport: Transport.RMQ,
          options: {
            urls: [`${env.get('RABBITMQ.URL')}`],
            queue: `${env.get('RABBITMQ.SERVICE.SONG')}`,
            queueOptions: {
              durable: false,
            },
          },
        }),
        inject: [EnvService],
      },
    ]),
  ],
  controllers: [AppController, SongController],
  providers: [
    // JwtService,
    // EnvService,
    PrismaService,
    LogService,
    {
      provide: APP_GUARD,
      useClass: JwtAuthGuard,
    },
    {
      provide: APP_GUARD,
      useClass: RolesGuard,
    },
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    // consumer.apply(TracingMiddleware).forRoutes('*');
    consumer
      .apply(TracingMiddleware)
      .forRoutes({ path: 'test', method: RequestMethod.GET });
  }
}
