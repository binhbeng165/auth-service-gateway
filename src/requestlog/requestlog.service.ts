import { Injectable } from '@nestjs/common';
import { PrismaService } from 'src/database/prisma.service';
import { CreateRequestLogDto } from './dto/create.dto';

@Injectable()
export class RequestlogService {
  constructor(private readonly prisma: PrismaService) {}
  public async addLog(data: any) {
    let criteria: CreateRequestLogDto = {
      url: data.url,
      method: data.method,
      req: JSON.stringify(data.req),
      res: JSON.stringify(data.res),
      statusCode: data.statusCode,
      dur: data.dur,
      headers: data.headers,
      timestamp: data['@timestamp'],
    };
    let x = await this.prisma.requestLog.create({ data: criteria });
    return x;
  }
}
