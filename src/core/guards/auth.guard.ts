import { ExecutionContext, Injectable } from '@nestjs/common';
import { IS_PUBLIC_KEY } from '../decorators';
import { Reflector } from '@nestjs/core';
import { AuthGuard } from '@nestjs/passport';

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {
  constructor(private reflector: Reflector) {
    super({
      property: 'userhehe',
      // the user object decoded from the token will be stored in the req.user property
    });
  }
  canActivate(context: ExecutionContext) {
    const isRpc = context.getType() === 'rpc';
    console.log(context.getType());
    const isPublic = this.reflector.getAllAndOverride<boolean>(IS_PUBLIC_KEY, [
      context.getHandler(), //method
      context.getClass(), // class
    ]);
    if (isPublic || isRpc) {
      return true;
    }
    return super.canActivate(context);
  }
}
